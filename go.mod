module github.com/masoud/todoapp

go 1.18

require (
	github.com/go-chi/chi v1.5.5 // indirect
	github.com/lib/pq v1.10.9
	github.com/thedevsaddam/renderer v1.2.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
