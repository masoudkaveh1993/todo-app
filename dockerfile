FROM golang:1.21.6

WORKDIR /app

COPY . .

RUN go get -d -v ./...

RUN go build -o api .

EXPOSE 9000

CMD [ "./api" ]


